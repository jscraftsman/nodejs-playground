
import BaseStat_CombatStat from '../util/basestat_combatstat.js';

const MIN_CONSTITUTION = 1;
const MAX_CONSTITUTION = 100;

const MIN_MAX_HP = 10;
const MAX_MAX_HP = 150;

// #########################
// # CONSTITUTION | MAX HP #
// #########################
// Formula: MAX HP			= (constitution - m) / b

// Ratio: 1 constitution	= 10 Max HP 
// Ratio: 100 constitution	= 150 Max HP

// Constraints: 10 >= Max HP <= 150 
export default class Constitution_MaxHP extends BaseStat_CombatStat {
	constructor() {
		let constraints = {
			MIN_BASE_STAT: MIN_CONSTITUTION,
			MAX_BASE_STAT: MAX_CONSTITUTION,
			MIN_COMBAT_STAT: MIN_MAX_HP,
			MAX_COMBAT_STAT: MAX_MAX_HP
		};
		super(constraints);
	}
}
