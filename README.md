# Creature Stats for RAMO

## Installation / Setup
- NodeJS must be installed
- Clone repository and run `npm install` to install the dependencies
- Use `npm test` to run unit test (mocha must be installed globally first `npm i -g mocha`)


## Links / Guides
- Graphing Tool - [link](https://www.desmos.com/calculator/slkjzmm3ly) (Used to confirm the values)
- Linear equations - [link](https://www.mathplanet.com/education/algebra-1/formulating-linear-equations/writing-linear-equations-using-the-slope-intercept-form) - (Used as guide in creating the formulas)
