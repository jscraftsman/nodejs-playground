
import StatConvertUtil from './stat-convert-util.js';

export default class BaseStat_CombatStat {
	constructor(constraints) {
		this.MIN_BASE_STAT = constraints.MIN_BASE_STAT;
		this.MIN_COMBAT_STAT = constraints.MIN_COMBAT_STAT;

		this.MAX_BASE_STAT = constraints.MAX_BASE_STAT;
		this.MAX_COMBAT_STAT = constraints.MAX_COMBAT_STAT;

		this._BASE_STAT = 0;
		this._COMBAT_STATE = 0;
	}

	set BaseStat(value) {
		this._BASE_STAT = value;

		// X Axis: Combat Stats (Max HP, Damage, Attack Delay, Dodge Chance, Critical chance)
		// Y Axis: Base Stats (Constitution, Strength, Dexterity, Agility, Luck)
		let stats =  new StatConvertUtil(
			this.MIN_COMBAT_STAT,							// X1
			this.MIN_BASE_STAT,								// Y1 
			this.MAX_COMBAT_STAT,							// X2
			this.MAX_BASE_STAT);							// Y2
		this._COMBAT_STATE = stats.convert(this._BASE_STAT);
	}

	get BaseStat()		{ return this._BASE_STAT;		}

	get CombatStat()	{ return this._COMBAT_STATE;	}
}
