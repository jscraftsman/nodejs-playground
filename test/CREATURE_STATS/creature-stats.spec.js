
import assert from 'assert';
import CreatureStats from '../../src/CREATURE_STATS/creature-stats.js';

describe('CreatureStats', function() {

	describe('Setup', function() {
		it('Should have initialized the class', function() {
			let cs = new CreatureStats();
			assert.notEqual(cs, undefined);
		});
	});

	describe('Constitution', function() {
		let cs = new CreatureStats();

		it('Should initialize the Constitution and Max HP value', function() {
			assert.equal(cs.Constitution, 0);
			assert.equal(cs.MaxHP, 0);

			let _constitution = 100;
			cs.Constitution = _constitution;

			assert.equal(cs.Constitution, _constitution);
		});

		it('Should set the Max HP value', function() {
			// CONSTITUTION: 1 | MAX HP: 10
			cs.Constitution = 1;
			assert.equal(cs.MaxHP, 10);

			// CONSTITUTION: 25 | MAX HP: 44
			cs.Constitution = 25;
			assert.equal(cs.MaxHP, 44);

			// CONSTITUTION: 50 | MAX HP: 79 
			cs.Constitution = 50;
			assert.equal(cs.MaxHP, 79);

			// CONSTITUTION: 75 | MAX HP: 114
			cs.Constitution = 75;
			assert.equal(cs.MaxHP, 115);

			// CONSTITUTION: 100 | MAX HP: 150 
			cs.Constitution = 100;
			assert.equal(cs.MaxHP, 150);
		});
	});

	describe('Strength', function() {
		let cs = new CreatureStats();

		it('Should set the strength and damage value', function() {
			assert.equal(cs.Strength, 0);
			assert.equal(cs.Damage, 0);

			let _strength = 100;
			cs.Strength = _strength;

			assert.equal(cs.Strength, _strength);
		});


		it('Should set the damage value', function() {
			// STRENGTH: 1 | DAMAGE: 1
			cs.Strength = 1;
			assert.equal(cs.Damage, 1);

			// STRENGTH: 25 | DAMAGE: 23
			cs.Strength = 25;
			assert.equal(cs.Damage, 23);

			// STRENGTH: 50 | DAMAGE: 45 
			cs.Strength = 50;
			assert.equal(cs.Damage, 45);
	
			// STRENGTH: 75 | DAMAGE:  68
			cs.Strength = 75;
			assert.equal(cs.Damage, 68);
	
			// STRENGTH: 100 | DAMAGE: 90
			cs.Strength = 100;
			assert.equal(cs.Damage, 90);
		});
	});

	describe('Dexterity', function() {
		let cs = new CreatureStats();

		it('Should set the dexterity value', function() {
			assert.equal(cs.Dexterity, 0);
			assert.equal(cs.AttackDelay, 0);

			let _dexterity = 100;
			cs.Dexterity = _dexterity;

			assert.equal(cs.Dexterity, _dexterity);
		});

		it('Should set the attack delay value', function() {
			// DEXTERITY = 1 | ATTACK DELAY = 3000 ms (3 seconds)
			cs.Dexterity = 1;
			assert.equal(cs.AttackDelay, 3000);

			// DEXTERITY = 25 | ATTACK DELAY = 2333 ms (2.3 seconds)
			cs.Dexterity = 25;
			assert.equal(cs.AttackDelay, 2333);

			// DEXTERITY = 50 | ATTACK DELAY = 1639 ms (1.6 seconds)
			cs.Dexterity = 50;
			assert.equal(cs.AttackDelay, 1639);

			// DEXTERITY = 75 | ATTACK DELAY = 944 ms (0.9 seconds)
			cs.Dexterity = 75;
			assert.equal(cs.AttackDelay, 944);

			// DEXTERITY = 100 | ATTACK DELAY = 250 ms (0.25 seconds)
			cs.Dexterity = 100;
			assert.equal(cs.AttackDelay, 250);
		});

	});

	describe('Agility', function() {
		let cs = new CreatureStats();

		it('Should set the agility and dodge chance value', function() {
			assert.equal(cs.Agility, 0);
			assert.equal(cs.DodgeChance, 0);

			let _agility = 100;
			cs.Agility = _agility;

			assert.equal(cs.Agility, _agility);
		});

		it('Should set the dodge chance value', function() {
			// AGILITY: 1 | DODGE CHANCE: 1%
			cs.Agility = 1;
			assert.equal(cs.DodgeChance, 1);

			// AGILITY: 25 | DODGE CHANCE: 13%
			cs.Agility = 25;
			assert.equal(cs.DodgeChance, 13);

			// AGILITY: 50 | DODGE CHANCE: 25%
			cs.Agility = 50;
			assert.equal(cs.DodgeChance, 25);

			// AGILITY: 75 | DODGE CHANCE: 38%
			cs.Agility = 75;
			assert.equal(cs.DodgeChance, 38);

			// AGILITY: 100 | DODGE CHANCE: 50%
			cs.Agility = 100;
			assert.equal(cs.DodgeChance, 50);
		});

	});

	describe('Luck', function() {
		let cs = new CreatureStats();

		it('Should set the luck and critical chance value', function() {
			assert.equal(cs.Luck, 0);
			assert.equal(cs.CriticalChance, 0);

			let _luck = 100;
			cs.Luck = _luck;

			assert.equal(cs.Luck, _luck);
		});

		it('Should set the critical chance value', function() {
			// LUCK: 1 | CRITICAL  CHANCE: 1%
			cs.Luck = 1;
			assert.equal(cs.CriticalChance, 1);

			// LUCK: 25 | CRITICAL  CHANCE: 19%
			cs.Luck = 25;
			assert.equal(cs.CriticalChance, 19);

			// LUCK: 50 | CRITICAL  CHANCE: 38%
			cs.Luck = 50;
			assert.equal(cs.CriticalChance, 38);

			// LUCK: 75 | CRITICAL  CHANCE: 56%
			cs.Luck = 75;
			assert.equal(cs.CriticalChance, 56);

			// LUCK: 100 | CRITICAL  CHANCE: 75%
			cs.Luck = 100;
			assert.equal(cs.CriticalChance, 75);

		});

	});
});
