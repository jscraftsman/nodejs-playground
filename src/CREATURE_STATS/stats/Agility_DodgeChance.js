
import BaseStat_CombatStat from '../util/basestat_combatstat.js';

const MIN_AGILITY = 1;
const MAX_AGILITY = 100;

const MIN_DODGE_CHANCE = 1;
const MAX_DODGE_CHANCE = 50;

// #######################################
// # AGILITY | DODGE CHANCE (percentage) #
// #######################################
// Formula: dodge chance	= (agility - b) / m 

// Ratio: 1 agility			= 1% chance to dodge
// Ratio: 100 agility		= 50% chance to dodge 

// Constraints: 1% >= Dodge Chance <= 50%
export default class Agility_DodgeChance extends BaseStat_CombatStat {
	constructor() {
		let constraints = {
			MIN_BASE_STAT: MIN_AGILITY,
			MAX_BASE_STAT: MAX_AGILITY,
			MIN_COMBAT_STAT: MIN_DODGE_CHANCE,
			MAX_COMBAT_STAT: MAX_DODGE_CHANCE
		};
		super(constraints);
	}
}
