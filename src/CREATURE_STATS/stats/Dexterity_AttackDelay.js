// #############
// # IMPORTANT #
// #############
// Compared to the other stats, the relationship between Dexterity (base stats) and Attack Delay (combat stats) is inversely proportional

import BaseStat_CombatStat from '../util/BaseStat_CombatStat.js';

const MIN_DEXTERITY = 1;
const MAX_DEXTERITY = 100;

const MIN_ATTACK_DELAY = 3000;
const MAX_ATTACK_DELAY = 250;

// ###########################################
// # DEXTERITY | ATTACK DELAY (milliseconds) #
// ###########################################
// Formula: attack delay	= (dexterity - m) / b

// Ratio: 1 dexterity		= 3000 (ms) attack delay
// Ratio: 100 dexterity		= 250 (ms) attack delay

// Constraints: 3000 ms (3 seconds) <= Attack Delay >= 250 ms
export default class Dexterity_AttackDelay extends BaseStat_CombatStat {
	constructor() {
		let constraints = {
			MIN_BASE_STAT: MIN_DEXTERITY,
			MAX_BASE_STAT: MAX_DEXTERITY,
			MIN_COMBAT_STAT: MIN_ATTACK_DELAY,
			MAX_COMBAT_STAT: MAX_ATTACK_DELAY
		};
		super(constraints);
	}
}
