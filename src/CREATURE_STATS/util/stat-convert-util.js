
export default class StatConvertUtil {
	constructor(_x1, _y1, _x2, _y2) {
		this.x1 = _x1;
		this.y1 = _y1;

		this.x2 = _x2;
		this.y2 = _y2;
	}

	convert(base_stat) {
		return _convert(base_stat, this.x1, this.y1, this.x2, this.y2);
	}
}

// #####################
// # PRIVATE FUNCTIONS #
// #####################

// Converts a base stats to its appropriate combat stats
function _convert(base_stat, x1, y1, x2, y2) {
	const m = calculateSlope(x1, y1, x2, y2);	// m (slope)
	const b = calculateIntercept(x1, y1, m);	// y-intercept

	let y = (base_stat - b) / m;
	return Math.round(y); 
}

// m = (y2 - y1) / (x2 - x1)
function calculateSlope(x1, y1, x2, y2) {
	return (y2 - y1) / (x2 - x1);
}

// y = mx + b
// b = y - mx
function calculateIntercept(x, y, m) {
	// return Y - intercept
	return (y - (m * x));}
