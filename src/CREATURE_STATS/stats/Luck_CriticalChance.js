
import BaseStat_CombatStat from '../util/BaseStat_CombatStat.js';

const MIN_LUCK = 1;
const MAX_LUCK = 100;

const MIN_CRITICAL_CHANCE = 1;
const MAX_CRITICAL_CHANCE = 75;

// #######################################
// # LUCK | CRITICAL CHANCE (percentage) #
// #######################################
// Formula: critical chance = (luck - b) / m 

// Ratio: 1 agility			= 1% chance to dodge
// Ratio: 100 agility		= 75% chance to dodge 

// Constraints: 1% >= Critical Chance < 75%
// Critical multiplier		= Times 2.5 Damage (base damage of creature)
export default class Luck_CriticalChance extends BaseStat_CombatStat {
	constructor() {
		let constraints = {
			MIN_BASE_STAT: MIN_LUCK,
			MAX_BASE_STAT: MAX_LUCK,
			MIN_COMBAT_STAT: MIN_CRITICAL_CHANCE,
			MAX_COMBAT_STAT: MAX_CRITICAL_CHANCE
		};
		super(constraints);
	}
}
