
import BaseStat_CombatStat from '../util/BaseStat_CombatStat.js';

const MIN_STRENGTH = 1;
const MAX_STRENGTH = 100;

const MIN_DAMAGE = 1;
const MAX_DAMAGE = 90;

// #####################
// # STRENGTH | DAMAGE #
// #####################
// Formula: damage			= (strength - m) / b

// Ratio: 1 strength		= 1 damage
// Ratio: 100 strength		= 90 damage

// Constraints: 10 >= Damage <= 90
export default class Strength_Damage extends BaseStat_CombatStat {
	constructor() {
		let constraints = {
			MIN_BASE_STAT: MIN_STRENGTH,
			MAX_BASE_STAT: MAX_STRENGTH,
			MIN_COMBAT_STAT: MIN_DAMAGE,
			MAX_COMBAT_STAT: MAX_DAMAGE
		};
		super(constraints);
	}
}
