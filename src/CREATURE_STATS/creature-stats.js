
import Constitution_MaxHP from './stats/Constitution_MaxHP.js';
import Strength_Damage from './stats/Strength_Damage.js';
import Dexterity_AttackDelay from './stats/Dexterity_AttackDelay.js';
import Agility_DodgeChance from './stats/Agility_DodgeChance.js';
import Luck_CriticalChance from './stats/Luck_CriticalChance.js';

export default class CreatureStats {

	constructor() {
		this._cmh = new Constitution_MaxHP();
		this._sd = new Strength_Damage();
		this._dad = new Dexterity_AttackDelay();
		this._adc = new Agility_DodgeChance();
		this._lcc = new Luck_CriticalChance();
	}

	// ############################
	// # Base Stats (Input Stats) #
	// ############################
	
	// Base Stats: Setters
	set Constitution(value) { this._cmh.BaseStat = value; }
	set Strength(value) { this._sd.BaseStat = value; }
	set Dexterity(value) { this._dad.BaseStat = value; }
	set Agility(value) { this._adc.BaseStat = value; }
	set Luck(value) { this._lcc.BaseStat = value; }

	// Base Stats: Getters
	get Constitution() { return this._cmh.BaseStat; }
	get Strength() { return this._sd.BaseStat; }
	get Dexterity() { return this._dad.BaseStat; }
	get Agility() { return this._adc.BaseStat; }
	get Luck() { return this._lcc.BaseStat; }

	// ###############################
	// # Combat Stats (Output Stats) #
	// ###############################
	get MaxHP()	{	return this._cmh.CombatStat;	}
	get Damage()	{	return this._sd.CombatStat;	}
	get AttackDelay()	{	return this._dad.CombatStat;	}
	get DodgeChance()	{	return this._adc.CombatStat;	}
	get CriticalChance()	{	return this._lcc.CombatStat;	}
}
